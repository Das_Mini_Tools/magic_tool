﻿using Extra.Utilities;
using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;

namespace magic_tool
{
    internal class Program
    {
        private static readonly string results = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "results_");
        private static readonly string target = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "target");

        private static void Error(string message)
        {
            Console.WriteLine(message);

            Console.WriteLine();

            Environment.Exit(1);
        }

        private static void Main(string[] args)
        {
            int length = 0;

            if (args.Length != 1)
                Error("magic_tool by Dasanko, written for catalinnc. Version " + Assembly.GetExecutingAssembly().GetName().Version);

            if (!Directory.Exists(target))
            {
                Directory.CreateDirectory(target);

                Error("No files found in the target directory.");
            }

            if (Regex.IsMatch(args[0], @"^0[xX][\da-fA-F]+$"))
                ProcessHexString(args[0]);
            else if (Regex.IsMatch(args[0], @"^\d+$"))
            {
                length = int.Parse(args[0]);

                ProcessInt(length);
            }
            else
                Error("Invalid parameter. The parameter must be a number in decimal or hexadecimal format.");
        }

        private static void ProcessHexString(string inputHex)
        {
            string outputFile = results + inputHex + ".txt";

            if (File.Exists(outputFile))
                File.Delete(outputFile);

            if (inputHex.ToLower().StartsWith("0x"))
                inputHex = inputHex.Substring(2);

            inputHex = inputHex.ToLower();

            Retry.Do(() =>
            {
                foreach (var file in Directory.EnumerateFiles(target, "*", SearchOption.AllDirectories))
                    using (var fs = File.OpenRead(file))
                    using (var br = new BinaryReader(fs))
                    {
                        byte[] magic = br.ReadBytes(inputHex.Length / 2);

                        string magicHex = BitConverter.ToString(magic).Replace("-", "").ToLower();

                        if (magicHex != inputHex)
                            continue;

                        string outputText = "0x" + magicHex.ToUpper() + ";" + file.Substring(target.Length + 1) + Environment.NewLine;

                        File.AppendAllText(outputFile, outputText);
                    }
            }, 100);
        }

        private static void ProcessInt(int length)
        {
            string outputFile = results + length + ".txt";

            if (File.Exists(outputFile))
                File.Delete(outputFile);

            Retry.Do(() =>
            {
                foreach (var file in Directory.EnumerateFiles(target, "*", SearchOption.AllDirectories))
                    using (var fs = File.OpenRead(file))
                    using (var br = new BinaryReader(fs))
                    {
                        byte[] magic = br.ReadBytes(length);

                        string magicHex = BitConverter.ToString(magic).Replace("-", "").ToLower();

                        string outputText = "0x" + magicHex + ";" + file.Substring(target.Length + 1) + Environment.NewLine;

                        File.AppendAllText(outputFile, outputText);
                    }
            }, 100);
        }
    }
}
